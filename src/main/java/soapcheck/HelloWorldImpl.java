
package soapcheck;

import javax.jws.WebService;

@WebService(endpointInterface = "soapcheck.HelloWorld")
public class HelloWorldImpl implements HelloWorld {

    public String sayHi(String text) {
        return "Hello " + text;
    }
}

